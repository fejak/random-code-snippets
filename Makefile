# Makefile for C/C++ programs in this repo

CC                 := gcc
CFLAGS             := -Wall -pedantic -Wextra -std=c99 -O0

CXX	           := g++
CXXFLAGS           := -Wall -pedantic -Wextra -std=c++03 -O0

SRC_DIR            := src/
C_DIR		   := c/
CPP_DIR            := cpp/
BIN_DIR            := bin/

HISTOGRAM          := histogramArray histogramVector
RECURSION          := countDigits decToBin fibonacci printArray
RANDOM             := sqrtApprox timeDiff trimTrailing
COMPLEX	           := complex

HISTOGRAM_DIR      := histogram/
RECURSION_DIR      := recursion/
RANDOM_DIR         := random/
COMPLEX_DIR        :=

HISTOGRAM_PATH_SRC := $(SRC_DIR)$(C_DIR)$(HISTOGRAM_DIR)
RECURSION_PATH_SRC := $(SRC_DIR)$(C_DIR)$(RECURSION_DIR)
RANDOM_PATH_SRC    := $(SRC_DIR)$(C_DIR)$(RANDOM_DIR)
COMPLEX_PATH_SRC   := $(SRC_DIR)$(CPP_DIR)$(COMPLEX_DIR)

HISTOGRAM_PATH_BIN := $(BIN_DIR)$(C_DIR)$(HISTOGRAM_DIR)
RECURSION_PATH_BIN := $(BIN_DIR)$(C_DIR)$(RECURSION_DIR)
RANDOM_PATH_BIN    := $(BIN_DIR)$(C_DIR)$(RANDOM_DIR)
COMPLEX_PATH_BIN   := $(BIN_DIR)$(CPP_DIR)$(COMPLEX_DIR)

all: $(HISTOGRAM) $(RECURSION) $(RANDOM) $(COMPLEX)

$(HISTOGRAM): % : $(HISTOGRAM_PATH_SRC)%.c
	mkdir -p $(HISTOGRAM_PATH_BIN)
	$(CC) $(CFLAGS) $^ -o $(HISTOGRAM_PATH_BIN)$@ -lm

$(RECURSION): % : $(RECURSION_PATH_SRC)%.c
	mkdir -p $(RECURSION_PATH_BIN)
	$(CC) $(CFLAGS) $^ -o $(RECURSION_PATH_BIN)$@ -lm

$(RANDOM): % : $(RANDOM_PATH_SRC)%.c
	mkdir -p $(RANDOM_PATH_BIN)
	$(CC) $(CFLAGS) $^ -o $(RANDOM_PATH_BIN)$@ -lm

$(COMPLEX): % : $(COMPLEX_PATH_SRC)%.cpp
	mkdir -p $(COMPLEX_PATH_BIN)
	$(CXX) $(CXXFLAGS) $^ -o $(COMPLEX_PATH_BIN)$@

clean:
	$(foreach bin, $(HISTOGRAM), rm -f $(BIN_DIR)$(C_DIR)$(HISTOGRAM_DIR)$(bin))
	$(foreach bin, $(RECURSION), rm -f $(BIN_DIR)$(C_DIR)$(RECURSION_DIR)$(bin))
	$(foreach bin, $(RANDOM), rm -f $(BIN_DIR)$(C_DIR)$(RANDOM_DIR)$(bin))
	$(foreach bin, $(COMPLEX), rm -f $(BIN_DIR)$(CPP_DIR)$(COMPLEX_DIR)$(bin))
	rm -rf $(BIN_DIR)$(C_DIR)$(HISTOGRAM_DIR)
	rm -rf $(BIN_DIR)$(C_DIR)$(RECURSION_DIR)
	rm -rf $(BIN_DIR)$(C_DIR)$(RANDOM_DIR)
	rm -rf $(BIN_DIR)$(CPP_DIR)$(COMPLEX_DIR)
	rm -rf $(BIN_DIR)

.PHONY: all clean
