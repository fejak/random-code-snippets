#!/usr/bin/env python3
#-------------------------------------------------------------------------------
import curses
import time
import random
import sys
#-------------------------------------------------------------------------------
WIN_HEIGHT = 20
WIN_WIDTH  = 70
PLUS  = 0
MINUS = 1
MULT  = 2
DIV   = 3
INIT_TIME = 30 # init time in seconds

OFS_OP_LEFT  = -12 # offset operand left
OFS_SIGN_OP  = -7  # offset sign operator
OFS_OP_RIGHT = -3  # offset operand right
OFS_SIGN_EQ  = 3   # offset sign equality
OFS_RES      = 6   # offset result
#===============================================================================
class Win:
    """
    General window class.
    Uses the border class for drawing its borders.
    """
    #---------------------------------------------------------------------------
    def __init__ ( self, window, yOfs, xOfs, yLen, xLen, yCenter, xCenter ):
        self . window = window
        
        ( self . yOfs,
          self . xOfs,
          self . yLen,
          self . xLen ) = self . __handleDimensions ( yOfs, xOfs,
                                                    yLen, xLen,
                                                    yCenter, xCenter )
        
        self . border = Border ( self )
    #---------------------------------------------------------------------------
    def __handleDimensions ( self, yOfs, xOfs, yLen, xLen, yCenter, xCenter ):
        """
        center is True => center the game window inside the terminal window
        """
        termYLen, termXLen = self . window . getmaxyx ()

        if xOfs >= termXLen or yOfs >= termYLen:
            raise ValueError ( "Offset larger than terminal size." )

        if yOfs + yLen >= termYLen:
            yLen = termYLen - yOfs
        if xOfs + xLen >= termXLen:
            xLen = termXLen - xOfs

        if yCenter and yLen < termYLen:
            yOfs = ( termYLen - yLen ) // 2
        if xCenter and xLen < termXLen:
            xOfs = ( termXLen - xLen ) // 2
        
        return yOfs, xOfs, yLen, xLen
    #---------------------------------------------------------------------------
    def drawBorder ( self ):
        self . border . draw ()
#===============================================================================
class MainWin ( Win ):
    """
    Main win class.
    Window which is always just horizontally centered.
    """
    #---------------------------------------------------------------------------
    def __init__ ( self, window, center, yLen, xLen ):
        super () . __init__ ( window, 0, 0, yLen, xLen, False, center )
        self . drawBorder ()
#===============================================================================
class Border:
    """
    Border around a window.
    """
    #---------------------------------------------------------------------------
    def __init__ ( self, win ):
        self . win = win
    #---------------------------------------------------------------------------
    def __drawHorizontalLine ( self, yShift ):
        for i in range ( 0, self . win . xLen ):
            self . win . window . addch ( self . win . yOfs + yShift,
                                          self . win . xOfs + i,
                                          curses . ACS_HLINE )
    #---------------------------------------------------------------------------
    def __drawUpperHorLine ( self ):
        self . __drawHorizontalLine ( 0 )
    #---------------------------------------------------------------------------
    def __drawLowerHorLine ( self ):
        self . __drawHorizontalLine ( self . win . yLen - 1 )
    #---------------------------------------------------------------------------
    def __drawVerticalLine ( self, xShift ):
        for i in range ( 0, self . win . yLen ):
            self . win . window . addch ( self . win . yOfs + i,
                                          self . win . xOfs + xShift,
                                          curses . ACS_VLINE )
    #---------------------------------------------------------------------------
    def __drawLeftVertLine ( self ):
        self . __drawVerticalLine ( 0 )
    #---------------------------------------------------------------------------
    def __drawRightVertLine ( self ):
        self . __drawVerticalLine ( self . win . xLen - 1 )
    #---------------------------------------------------------------------------
    def __drawCorners ( self ):
        self . win . window . addch ( self . win . yOfs,
                                      self . win . xOfs,
                                      curses . ACS_ULCORNER )
        self . win . window . addch ( self . win . yOfs,
                                      self . win . xOfs + self . win . xLen - 1,
                                      curses . ACS_URCORNER )
        self . win . window . addch ( self . win . yOfs + self . win . yLen - 1,
                                      self . win . xOfs,
                                      curses . ACS_LLCORNER )
        self . win . window . addch ( self . win . yOfs + self . win . yLen - 1,
                                      self . win . xOfs + self . win . xLen - 1,
                                      curses . ACS_LRCORNER )
    #---------------------------------------------------------------------------
    def draw ( self ):
        self . __drawUpperHorLine  ()
        self . __drawLowerHorLine  ()
        self . __drawLeftVertLine  ()
        self . __drawRightVertLine ()
        self . __drawCorners       ()
#===============================================================================
class Question:
    #---------------------------------------------------------------------------
    signs = [ "+", "-", "*", ":" ]
    #---------------------------------------------------------------------------
    def __init__ ( self, win ):
        self . win = win
        self . currResult = None # result entered by the user
        self . res = None        # reference result
    #---------------------------------------------------------------------------
    def __drawStr ( self, string, xOfs ):
        # // ... integer division
        self . win . window . addstr ( self . win . yOfs
                                       + self . win . yLen // 2,
                                       self . win . xOfs
                                       + self . win . xLen // 2 + xOfs,
                                       string )
    #---------------------------------------------------------------------------
    def __drawSigns ( self, signNr ):
        # draw the operation sign
        self . __drawStr ( Question . signs [signNr], OFS_SIGN_OP )
        # draw the equality sign
        self . __drawStr ( "=", OFS_SIGN_EQ )
    #---------------------------------------------------------------------------
    def __leftStrip ( self, n ):
        if n != 0:
            n = str ( n ) . lstrip ( "0" )
        else:
            n = str ( n )
        return n
    #---------------------------------------------------------------------------
    def __drawNumbers ( self, n1, n2 ):
        n1 = self . __leftStrip ( n1 )
        n2 = self . __leftStrip ( n2 )
        self . __drawStr ( n1, OFS_OP_LEFT )
        self . __drawStr ( n2, OFS_OP_RIGHT )
    #---------------------------------------------------------------------------
    def __drawResult ( self ):
        self . __drawStr ( "   ", OFS_RES ) # ???
        string = "" if self . currResult == None else str ( self . currResult )
        self . __drawStr ( string, OFS_RES )
    #---------------------------------------------------------------------------
    def __generateAddition ( self ):
        n1 = random . randint ( 0, 100 )
        n2 = random . randint ( 0, 100 ) 
        res = n1 + n2
        return n1, n2, res
    #---------------------------------------------------------------------------
    def __generateSubtraction ( self ):
        n1 = random . randint ( 0, 200 )
        n2 = random . randint ( 0, n1 ) 
        res = n1 - n2
        return n1, n2, res
    #---------------------------------------------------------------------------
    def __generateMultiplication ( self ):
        n1 = random . randint ( 0, 20 )
        n2 = random . randint ( 0, 20 ) 
        res = n1 * n2
        return n1, n2, res
    #---------------------------------------------------------------------------
    def __generateDivision ( self ):
        res = random . randint ( 1, 20 )
        n2  = random . randint ( 1, 100 // res )
        n1 = res * n2
        return n1, n2, res
    #---------------------------------------------------------------------------
    def __draw ( self, n1, n2, signNr ):
        self . __drawNumbers ( n1, n2 )
        self . __drawSigns ( signNr )
        self . __drawResult ()
    #---------------------------------------------------------------------------
    def __clear ( self ):
        self . __drawStr ( "   ", OFS_OP_LEFT )
        self . __drawStr ( "   ", OFS_OP_RIGHT )
        self . __drawStr ( "   ", OFS_RES )
    #---------------------------------------------------------------------------
    def generateQuestion ( self ):
        signNr = random . randint ( 0, 3 )
        n1 = 0
        n2 = 0

        funcs = [ self . __generateAddition,
                  self . __generateSubtraction,
                  self . __generateMultiplication,
                  self . __generateDivision ]

        n1, n2, self . res = funcs [signNr] ()

        self . __clear ()
        self . __draw ( n1, n2, signNr )
    #---------------------------------------------------------------------------
    def addNumber ( self, number ):
        if self . currResult == None:
            self . currResult = number
        else:
            if self . currResult < 100:
                self . currResult = self . currResult * 10 + number
    #---------------------------------------------------------------------------
    def delNumber ( self ):
        if self . currResult == None:
            return
        if self . currResult < 10:
            self . currResult = None
        else:
            self . currResult //= 10
    #---------------------------------------------------------------------------
    def handleKey ( self, keyCode ):
        if keyCode > 47 and keyCode < 58: # digits
            self . addNumber ( keyCode - 48 )
        if keyCode == 263: # backspace
            self . delNumber ()
        if keyCode == 10: # enter
            if self . currResult == self . res:
                self . currResult = None
                self . generateQuestion ()
                return True
            self . currResult = None
        self . __drawResult () # refresh the result
        return False
#===============================================================================
class Counter:
    #---------------------------------------------------------------------------
    def __init__ ( self, win ):
        self . win  = win
        self . time = INIT_TIME * 10 # seconds
    #---------------------------------------------------------------------------
    def refresh ( self, i ):
        if i % 10 == 0:
            self . time -= 1
        string = str ( self . time // 10 )
        string += "."
        string += str ( self . time % 10 )
        self . win . window . addstr ( self . win . yOfs + 2,
                                       self . win . xOfs
                                       + self . win . xLen // 2 - 2,
                                       string )
        return self . time
    #---------------------------------------------------------------------------
    def add ( self, secs ):
        self . time += secs * 10
#===============================================================================
def main ( stdscr ):
    curses . curs_set ( 0 )
    stdscr . clear ()
    random . seed ()

    win = MainWin ( stdscr, True, WIN_HEIGHT, WIN_WIDTH )
    qst = Question ( win )
    cntr = Counter ( win )

    i = 0
    correctCnt = 0

    qst . generateQuestion ()

    # main game loop
    while True:
        if i == 100:
            i = 0
        if cntr . refresh ( i ) == 0:
            break
        stdscr . refresh ()
        stdscr . timeout ( 0 )
        if qst . handleKey ( stdscr . getch () ):
            cntr . add ( 5 )
            correctCnt += 1
        time . sleep ( .01 )
        i += 1
    
    return correctCnt
#-------------------------------------------------------------------------------
def start ():
    try:
        correctCnt = curses . wrapper ( main )
    except KeyboardInterrupt:
        print ( "Program interrupted." )
        sys . exit ( 1 )
    print ( "Number of correct answers: {}" . format ( correctCnt ) )
#-------------------------------------------------------------------------------
if __name__ == "__main__":
    start ()
