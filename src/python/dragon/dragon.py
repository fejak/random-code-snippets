#!/usr/bin/env python3
#-------------------------------------------------------------------------------
# generate dragon curves
# https://en.wikipedia.org/wiki/Dragon_curve
# https://www.cs.swarthmore.edu/~soni/cs21/f16/Labs/lab09.php
#-------------------------------------------------------------------------------
import sys # exit, argv
#-------------------------------------------------------------------------------
def flip ( lst ):
    # flips 'L' => 'R' and 'R' => 'L'
    lstNew = []
    for x in lst:
        if x == 'L':
            lstNew . append ( 'R' )
        if x == 'R':
            lstNew . append ( 'L' )
    return lstNew
#-------------------------------------------------------------------------------
def dragonRec ( lst, n ):
    if n == 0:
        return lst

    lst . append ( 'L' )
    # flip the list without the last 'L' + reverse the order of this list
    lst . extend ( flip ( lst[:-1] ) [::-1] )
    return dragonRec ( lst, n - 1 )
#-------------------------------------------------------------------------------
def dragon ( n ):
    lst = []
    return dragonRec ( lst, n )
#-------------------------------------------------------------------------------
def main ():
    try:
        if len ( sys . argv ) < 2:
            print ( "Usage: {} [number_of_iterations]"
                     . format ( sys . argv [0] ) )
            sys . exit ( 1 )
        try:
            sys . argv [1] = int ( sys . argv [1] )
        except ValueError:
            print ( "Enter an integer." )
            sys . exit ( 1 )
        for i in range ( int ( sys . argv [1] ) + 1 ):
            print ( "{}: {}" . format ( i, dragon ( i ) ) )
    except KeyboardInterrupt:
        print ( "\nProgram interrputed. Exiting..." )
        sys . exit ( 1 )
#-------------------------------------------------------------------------------
if __name__ == "__main__":
    main ()
#-------------------------------------------------------------------------------
