/*----------------------------------------------------------------------------*/
/*
 * Print the vertical and/or horizontal histogram of frequency of characters
 * in input.
 * As far as whitespace characters, only spaces, tabs and newlines are graphed.
 * This is a classic exercise presented e.g. in the legendary K&R book.
 *
 * This is the "simple" version using a statically allocated array.
 *
 * Different modes of graphing can be choosed by using
 * the LINEAR / LOGARITHMIC / ONE_TO_ONE scaling constants.
 *
 * Compiled with:
 *   'gcc -Wall -pedantic -Wextra -std=c89 -O0 histogramArray.c -lm'
 *
 * Author: gitlab.com/fejak
 */
/*----------------------------------------------------------------------------*/
#include <stdio.h>    /*  stdin, fgetc, printf, fopen, fclose */
#include <stdlib.h>   /*  EXIT_SUCCESS, malloc, free */
#include <errno.h>    /*  errno */
#include <string.h>   /*  strerror */
#include <math.h>     /*  log */
/*----------------------------------------------------------------------------*/
#define ARR_SIZE        255
/*----------------------------------------------------------------------------*/
#define BOOL            int
#define TRUE            1
#define FALSE           0
/*----------------------------------------------------------------------------*/
#define MAX_HEIGHT      50    /* Maximum height of the histogram */
#define MAX_WIDTH       100   /* Maximum width of the histogram */
/*----------------------------------------------------------------------------*/
#define LINEAR          0     /* Linear graphing - respects the
                                 above-defined dimensions */
#define LOGARITHMIC     1     /* Logarithmic graphing - respects the
                                 above-defined dimensions */
#define ONE_TO_ONE      2     /* 1:1 graphing - 1 dash = 1 character occurence
                                 => does not respect the dimensions */
/*----------------------------------------------------------------------------*/
#define GRAPHING        LOGARITHMIC  /* LINEAR / LOGARITHMIC / ONE_TO_ONE */
/*----------------------------------------------------------------------------*/
/**
 * Check whether a character is "addable" into the vector.
 */
BOOL            addableChar             ( char                    c )
{
  return ( c > 32 && c < 127 ) || c == ' ' || c == '\t' || c == '\n';
}
/*----------------------------------------------------------------------------*/
/**
 * Check whether to read from stdin or from a file.
 */
BOOL            handleInput             ( int           const     argc,
                                          char                  * argv [],
                                          FILE                 ** input )
{
  if ( argc < 2 )
  {
    * input = stdin;
    return TRUE;
  }
  else
  {
    return ( * input = fopen ( argv [1], "r" ) ) != NULL;
  }
}
/*----------------------------------------------------------------------------*/
/**
 * Check whether a character is valid and potentially add it into the array.
 * Valid character    => was added      => returns TRUE.
 * Invalid character  => was not added  => return FALSE.
 */
BOOL            addChar                 ( int                     arr [],
                                          int           const     c )
{
  if ( addableChar ( c ) )
  {
    ++ arr [c];
    return TRUE;
  }
  return FALSE;
}
/*----------------------------------------------------------------------------*/
/**
 * Read characters into the vector from an input.
 */
void            readInput               ( FILE                  * inFile,
                                          int                     arr [] )
{
  char c;

  while ( TRUE )
  {
    c = fgetc ( inFile );

    if ( c == EOF )
    {
      break;
    }

    addChar ( arr, c );
  }
}
/*----------------------------------------------------------------------------*/
/**
 * Find maximum frequency value in the vector.
 */
int             findMax                 ( int           const     arr [] )
{
  int i;
  int max = 0;

  for ( i = 0; i < ARR_SIZE; ++i )
  {
    if ( arr [i] > max )
    {
      max = arr [i];
    }
  }

  return max;
}
/*----------------------------------------------------------------------------*/
/**
 * Return correct dimensions of the graph according to the settings / flags.
 */
int             getDimension            ( int           const     max,
                                          int           const     maxConst )
{
  if ( GRAPHING == ONE_TO_ONE )
  {
    return max;
  }
  else /* GRAPHING == LINEAR || GRAPHING == LOGARITHMIC */
  {
    return maxConst;
  }
}
/*----------------------------------------------------------------------------*/
/**
 * Return the quantity represented by one dash character.
 */
double          getOneSize              ( int           const     max,
                                          int           const     maxConst )
{
  if ( GRAPHING == ONE_TO_ONE )
  {
    return 1;
  }
  else /* GRAPHING == LINEAR || GRAPHING == LOGARITHMIC */
  {
    return (double) max / maxConst;
  }
}
/*----------------------------------------------------------------------------*/
/**
 * Get the length of a histogram line.
 * (Used also for vertical graphing.)
 */
int             getLength               ( int           const     freq,
                                          int           const     max,
                                          int           const     maxConst )
{
  int    dimension = getDimension ( max, maxConst );
  double oneSize   = getOneSize   ( max, maxConst );

  if ( GRAPHING == LINEAR )
    return (int) ( freq / oneSize );
  else
  if ( GRAPHING == LOGARITHMIC )
  {
    /* Handling special cases for the log calculation */
    if ( max == 1 )
      return dimension;
    if ( freq == 1 )
      return (int) ( log ( 2 ) / log ( max ) * dimension ) / 2;
    else
      return (int) ( log ( freq ) / log ( max ) * dimension );
  }
  else
  if ( GRAPHING == ONE_TO_ONE )
    return freq;
  else
    return -1;  /* Wrong GRAPHING value */
}
/*----------------------------------------------------------------------------*/
/**
 * Draw (horizontal) line.
 */
BOOL            drawLine                ( int           const     freq,
                                          int           const     max )
{
  int i;
  int length = getLength ( freq, max, MAX_WIDTH );

  if ( length < 0 )
  {
    printf ( "Error: Illegal GRAPHING value!\n" );
    return FALSE;
  }

  for ( i = 0; i < length; ++i )
  {
    putchar ( '-' );
  }

  printf ( "| %d\n", freq );

  return TRUE;
}
/*----------------------------------------------------------------------------*/
/**
 * Handle the drawing of special characters.
 */
void            drawCharacter           ( char          const     c )
{
  switch ( c )
  {
    case '\t':
      printf ( "\\t" );
      break;
    case '\n':
      printf ( "\\n" );
      break;
    default:
      printf ( "%c ", c );
  }
}
/*----------------------------------------------------------------------------*/
/**
 * Draw histogram horizontally.
 */
BOOL            drawHistogramHor        ( int           const     arr [] )
{
  int i;
  int max;      /* Max frequency in the vector for scale */

  max = findMax ( arr );

  for ( i = 0; i < ARR_SIZE; ++i )
  {
    if ( arr [i] > 0 )
    {
      drawCharacter ( i );
      if ( ! drawLine ( arr [i], max ) )
        return FALSE;
    }
  }

  return TRUE;
}
/*----------------------------------------------------------------------------*/
/**
 * Handle the drawing of given vertical character.
 * This function checks whether a character can be drawn according to its
 * x/y coordinates.
 */
BOOL            handleDrawCharVer       ( int           const     y,
                                          int           const     x,
                                          int           const     arr [],
                                          int           const     max )
{
  int length = getLength ( arr [x], max, MAX_HEIGHT );

  if ( arr [x] == 0 )
    return TRUE;

  if ( length < 0 )
  {
    printf ( "Error: Illegal GRAPHING value!\n" );
    return FALSE;
  }

  if ( y <= length )
    putchar ( '|' );
  else
    putchar ( ' ' );

  if ( x < ARR_SIZE - 1 )
    putchar ( ' ' );

  return TRUE;
}
/*----------------------------------------------------------------------------*/
/**
 * Draw histogram vertically.
 */
BOOL            drawHistogramVer        ( int           const     arr [] )
{
  int y, x;
  int max;      /* Max frequency in the vector for scale */
  int height;   /* Height of the graph */

  max    = findMax ( arr );
  height = getDimension ( max, MAX_HEIGHT );

  if ( max == 0 ) /* Empty input */
    return TRUE;

  for ( y = height; y > 0; --y )
  {
    for ( x = 0; x < ARR_SIZE; ++x )
    {
      if ( ! handleDrawCharVer ( y, x, arr, max ) )
        return FALSE;
    }
    putchar ( '\n' );
  }

  for ( x = 0; x < ARR_SIZE; ++x )
  {
    if ( arr [x] > 0 )
      drawCharacter ( x );
  }
  putchar ( '\n' );

  return TRUE;
}
/*----------------------------------------------------------------------------*/
BOOL            drawHistogram           ( int           const     arr [] )
{
  return drawHistogramHor ( arr )
      && drawHistogramVer ( arr );
}
/*----------------------------------------------------------------------------*/
void            handleClose             ( FILE                 ** inFile )
{
  if ( * inFile != stdin )
  {
    fclose ( * inFile );
    * inFile = NULL;
  }
}
/*----------------------------------------------------------------------------*/
int             main                    ( int                     argc,
                                          char                  * argv [] )
{
  FILE  * inFile = NULL;
  int     arr [ARR_SIZE] = {0};

  if ( ! handleInput ( argc, argv, & inFile ) )
  {
    printf ( "Error: %s\n", strerror ( errno ) );
    return EXIT_FAILURE;
  }

  readInput ( inFile, arr );

  handleClose ( & inFile );

  if ( ! drawHistogram ( arr ) )
  {
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
/*----------------------------------------------------------------------------*/
