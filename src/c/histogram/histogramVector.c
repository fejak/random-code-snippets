/*----------------------------------------------------------------------------*/
/*
 * Print the vertical and/or horizontal histogram of frequency of characters
 * in input.
 * As far as whitespace characters, only spaces, tabs and newlines are graphed.
 * This is a classic exercise presented e.g. in the legendary K&R book.
 *
 * This is the "heavy-weight" version using a resizable vector implementation.
 * This is of course redundant for simple ASCII character parsing.
 *
 * Different modes of graphing can be choosed by using
 * the LINEAR / LOGARITHMIC / ONE_TO_ONE scaling constants.
 *
 * Compiled with:
 *   'gcc -Wall -pedantic -Wextra -std=c89 -O0 histogramVector.c -lm'
 *
 * Author: gitlab.com/fejak
 */
/*----------------------------------------------------------------------------*/
#include <stdio.h>    /*  stdin, fgetc, printf, fopen, fclose */
#include <stdlib.h>   /*  EXIT_SUCCESS, malloc, free */
#include <errno.h>    /*  errno */
#include <string.h>   /*  strerror */
#include <math.h>     /*  log */
/*----------------------------------------------------------------------------*/
#define INIT_SIZE       1
#define RESIZE_FACTOR   2
/*----------------------------------------------------------------------------*/
#define BOOL            int
#define TRUE            1
#define FALSE           0
/*----------------------------------------------------------------------------*/
#define MAX_HEIGHT      50    /* Maximum height of the histogram */
#define MAX_WIDTH       100   /* Maximum width of the histogram */
/*----------------------------------------------------------------------------*/
#define LINEAR          0     /* Linear graphing - respects the
                                 above-defined dimensions */
#define LOGARITHMIC     1     /* Logarithmic graphing - respects the
                                 above-defined dimensions */
#define ONE_TO_ONE      2     /* 1:1 graphing - 1 dash = 1 character occurence
                                 => does not respect the dimensions */
/*----------------------------------------------------------------------------*/
#define GRAPHING        LOGARITHMIC  /* LINEAR / LOGARITHMIC / ONE_TO_ONE */
/*----------------------------------------------------------------------------*/
/**
 * "Type character-frequency"
 */
typedef struct TCharFreq
{
  char  m_Char;
  int   m_Freq;
} TCharFreq;
/*----------------------------------------------------------------------------*/
/**
 * "Type character-frequency vector"
 */
typedef struct TCharFreqVec
{
  TCharFreq * m_Ptr;
  int         m_Alloc;
  int         m_Len;
} TCharFreqVec;
/*----------------------------------------------------------------------------*/
void            charFreqVecInit         ( TCharFreqVec          * vec )
{
  vec -> m_Ptr   = NULL;
  vec -> m_Alloc = 0;
  vec -> m_Len   = 0;
}
/*----------------------------------------------------------------------------*/
void            charFreqVecFree         ( TCharFreqVec          * vec )
{
  free ( vec -> m_Ptr );
}
/*----------------------------------------------------------------------------*/
void            charFreqVecReset        ( TCharFreqVec          * vec )
{
  charFreqVecFree ( vec );
  charFreqVecInit ( vec );
}
/*----------------------------------------------------------------------------*/
/**
 * Check whether a character is "addable" into the vector.
 */
BOOL            addableChar             ( char                    c )
{
  return ( c > 32 && c < 127 ) || c == ' ' || c == '\t' || c == '\n';
}
/*----------------------------------------------------------------------------*/
/**
 * Handle allocation of the character-frequency vector
 */
void            charFreqVecAlloc        ( TCharFreqVec          * vec )
{
  if ( vec -> m_Alloc == 0 )
  {
    vec -> m_Alloc = INIT_SIZE;
    vec -> m_Ptr = malloc ( sizeof ( * ( vec -> m_Ptr ) ) * vec -> m_Alloc );
  }
  else
  if ( vec -> m_Alloc == vec -> m_Len )
  {
    vec -> m_Alloc *= RESIZE_FACTOR;
    vec -> m_Ptr = realloc ( vec -> m_Ptr,
                              sizeof ( * ( vec -> m_Ptr ) ) * vec -> m_Alloc );
  }
}
/*----------------------------------------------------------------------------*/
/**
 * Add a character into the histogram vector.
 * A naive vector implementation => every addition has O(n) time complexity.
 */
void            charFreqVecAdd          ( TCharFreqVec          * vec,
                                          char                    c )
{
  int i;

  if ( ! addableChar ( c ) )
    return;

  for ( i = 0; i < vec -> m_Len; ++i )
  {
    if ( vec -> m_Ptr [i] . m_Char == c )
    {
      ++ ( vec -> m_Ptr [i] . m_Freq );
      return;
    }
  }

  charFreqVecAlloc ( vec );

  vec -> m_Ptr [vec -> m_Len] . m_Char = c;
  vec -> m_Ptr [vec -> m_Len] . m_Freq = 1;
  ++ ( vec -> m_Len );
}
/*----------------------------------------------------------------------------*/
/**
 * Check whether to read from stdin or from a file.
 */
BOOL            handleInput             ( int           const     argc,
                                          char                  * argv [],
                                          FILE                 ** input )
{
  if ( argc < 2 )
  {
    * input = stdin;
    return TRUE;
  }
  else
  {
    return ( * input = fopen ( argv [1], "r" ) ) != NULL;
  }
}
/*----------------------------------------------------------------------------*/
/**
 * Read characters into the vector from an input.
 */
void            readInput               ( FILE                  * inFile,
                                          TCharFreqVec          * vec )
{
  char c;

  while ( TRUE )
  {
    c = fgetc ( inFile );

    if ( c == EOF )
    {
      break;
    }

    charFreqVecAdd ( vec, c );
  }
}
/*----------------------------------------------------------------------------*/
/**
 * Find maximum frequency value in the vector.
 */
int             findMax                 ( TCharFreqVec  const   * vec )
{
  int i;
  int max = 0;

  for ( i = 0; i < vec -> m_Len; ++i )
  {
    if ( vec -> m_Ptr [i] . m_Freq > max )
    {
      max = vec -> m_Ptr [i] . m_Freq;
    }
  }

  return max;
}
/*----------------------------------------------------------------------------*/
/**
 * Return correct dimensions of the graph according to the settings / flags.
 */
int             getDimension            ( int           const     max,
                                          int           const     maxConst )
{
  if ( GRAPHING == ONE_TO_ONE )
  {
    return max;
  }
  else /* GRAPHING == LINEAR || GRAPHING == LOGARITHMIC */
  {
    return maxConst;
  }
}
/*----------------------------------------------------------------------------*/
/**
 * Return the quantity represented by one dash character.
 */
double          getOneSize              ( int           const     max,
                                          int           const     maxConst )
{
  if ( GRAPHING == ONE_TO_ONE )
  {
    return 1;
  }
  else /* GRAPHING == LINEAR || GRAPHING == LOGARITHMIC */
  {
    return (double) max / maxConst;
  }
}
/*----------------------------------------------------------------------------*/
/**
 * Get the length of a histogram line.
 * (Used also for vertical graphing.)
 */
int             getLength               ( int           const     freq,
                                          int           const     max,
                                          int           const     maxConst )
{
  int    dimension = getDimension ( max, maxConst );
  double oneSize   = getOneSize   ( max, maxConst );

  if ( GRAPHING == LINEAR )
    return (int) ( freq / oneSize );
  else
  if ( GRAPHING == LOGARITHMIC )
  {
    /* Handling special cases for the log calculation */
    if ( max == 1 )
      return dimension;
    if ( freq == 1 )
      return (int) ( log ( 2 ) / log ( max ) * dimension ) / 2;
    else
      return (int) ( log ( freq ) / log ( max ) * dimension );
  }
  else
  if ( GRAPHING == ONE_TO_ONE )
    return freq;
  else
    return -1;  /* Wrong GRAPHING value */
}
/*----------------------------------------------------------------------------*/
/**
 * Draw (horizontal) line.
 */
BOOL            drawLine                ( int           const     freq,
                                          int           const     max )
{
  int i;
  int length = getLength ( freq, max, MAX_WIDTH );

  if ( length < 0 )
  {
    printf ( "Error: Illegal GRAPHING value!\n" );
    return FALSE;
  }

  for ( i = 0; i < length; ++i )
  {
    putchar ( '-' );
  }

  printf ( "| %d\n", freq );

  return TRUE;
}
/*----------------------------------------------------------------------------*/
/**
 * Handle the drawing of special characters.
 */
void            drawCharacter           ( TCharFreqVec  const   * vec,
                                          int           const     idx )
{
  switch ( vec -> m_Ptr [idx] . m_Char )
  {
    case '\t':
      printf ( "\\t" );
      break;
    case '\n':
      printf ( "\\n" );
      break;
    default:
      printf ( "%c ", vec -> m_Ptr [idx] . m_Char );
  }
}
/*----------------------------------------------------------------------------*/
/**
 * Draw histogram horizontally.
 */
BOOL            drawHistogramHor        ( TCharFreqVec  const   * vec )
{
  int i;
  int max;      /* Max frequency in the vector for scale */

  max = findMax ( vec );

  for ( i = 0; i < vec -> m_Len; ++i )
  {
    drawCharacter ( vec, i );
    if ( ! drawLine ( vec -> m_Ptr [i] . m_Freq, max ) )
      return FALSE;
  }

  return TRUE;
}
/*----------------------------------------------------------------------------*/
/**
 * Handle the drawing of given vertical character.
 * This function checks whether a character can be drawn according to its
 * x/y coordinates.
 */
BOOL            handleDrawCharVer       ( int           const     y,
                                          int           const     x,
                                          TCharFreqVec  const   * vec,
                                          int           const     max )
{
  int length = getLength ( vec -> m_Ptr [x] . m_Freq, max, MAX_HEIGHT );

  if ( length < 0 )
  {
    printf ( "Error: Illegal GRAPHING value!\n" );
    return FALSE;
  }

  if ( y <= length )
    putchar ( '|' );
  else
    putchar ( ' ' );

  if ( x < vec -> m_Len - 1 )
    putchar ( ' ' );

  return TRUE;
}
/*----------------------------------------------------------------------------*/
/**
 * Draw histogram vertically.
 */
BOOL            drawHistogramVer        ( TCharFreqVec  const   * vec )
{
  int y, x;
  int max;      /* Max frequency in the vector for scale */
  int height;   /* Height of the graph */

  max    = findMax ( vec );
  height = getDimension ( max, MAX_HEIGHT );

  if ( max == 0 ) /* Empty input */
    return TRUE;

  for ( y = height; y > 0; --y )
  {
    for ( x = 0; x < vec -> m_Len; ++x )
    {
      if ( ! handleDrawCharVer ( y, x, vec, max ) )
        return FALSE;
    }
    putchar ( '\n' );
  }

  for ( x = 0; x < vec -> m_Len; ++x )
  {
    drawCharacter ( vec, x );
  }
  putchar ( '\n' );

  return TRUE;
}
/*----------------------------------------------------------------------------*/
BOOL            drawHistogram           ( TCharFreqVec  const   * vec )
{
  return drawHistogramHor ( vec )
      && drawHistogramVer ( vec );
}
/*----------------------------------------------------------------------------*/
void            handleClose             ( FILE                 ** inFile )
{
  if ( * inFile != stdin )
  {
    fclose ( * inFile );
    * inFile = NULL;
  }
}
/*----------------------------------------------------------------------------*/
int             main                    ( int                     argc,
                                          char                  * argv [] )
{
  FILE * inFile = NULL;
  TCharFreqVec vec;

  if ( ! handleInput ( argc, argv, & inFile ) )
  {
    printf ( "Error: %s\n", strerror ( errno ) );
    return EXIT_FAILURE;
  }

  charFreqVecInit ( & vec );

  readInput ( inFile, & vec );

  handleClose ( & inFile );

  if ( ! drawHistogram ( & vec ) )
  {
    charFreqVecReset ( & vec );
    return EXIT_FAILURE;
  }

  charFreqVecReset ( & vec );

  return EXIT_SUCCESS;
}
/*----------------------------------------------------------------------------*/
