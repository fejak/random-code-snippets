/*
    Column cutter.
    Cut the m-th to n-th column of a text file, inclusive.

    Author: gitlab.com/fejak
    Compilable with 'gcc -Wall -pedantic -Wextra -std=c89'
*/
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#define BOOL        int
#define TRUE        1
#define FALSE       0
/*----------------------------------------------------------------------------*/
BOOL isDigit ( char c )
{
    return c > 47 && c < 58;
}
/*----------------------------------------------------------------------------*/
BOOL strToInt ( char const * str, int * number )
{
    int i;
    int strLen = strlen ( str );
    * number = 0;

    for ( i = 0; i < strLen; ++i )
    {
        if ( ! isDigit ( str [i] ) )
            return FALSE;

        * number = * number * 10 + str [i] - 48;
    }

    return TRUE;
}
/*----------------------------------------------------------------------------*/
int main ( int argc, char * argv [] )
{
    int c;
    int colCnt = 0;

    int startCol;
    int stopCol;

    FILE * inFile = stdin;

    if ( argc < 3 || argc > 4 )
    {
        printf ( "Usage: cutter [filename] <start_column> <end_column>\n" );
        return EXIT_FAILURE;
    }

    if ( argc == 3 )
    {
        if ( ! strToInt ( argv [1], & startCol )
          || ! strToInt ( argv [2], & stopCol ) )
        {
            printf ( "Column number not correct. Enter a positive decimal integer.\n" );
            return EXIT_FAILURE;
        }
    }
    else /* argc == 4 */
    {
        if ( ( inFile = fopen ( argv [1], "r" ) ) == NULL )
        {
            printf ( "Can't open the file.\n" );
            printf ( "Usage: cutter [filename] <start_column> <end_column>\n" );
            return EXIT_FAILURE;
        }

        if ( ! strToInt ( argv [2], & startCol )
          || ! strToInt ( argv [3], & stopCol ) )
        {
            printf ( "Column number not correct. Enter a positive decimal integer.\n" );
            return EXIT_FAILURE;
        }
    }

    if ( startCol > stopCol )
        return EXIT_SUCCESS;

    while ( ( c = getc ( inFile ) ) != EOF )
    {
        ++ colCnt;

        if ( c == '\n' )
        {
            printf ( "\n" );
            colCnt = 0;
        }
        else
        if ( colCnt >= startCol && colCnt <= stopCol )
        {
            printf ( "%c", c );
        }
    }

    if ( inFile != stdin )
        fclose ( inFile );

    return EXIT_SUCCESS;
}
