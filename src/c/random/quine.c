/*
 * Quine - a program that outputs its own source code.
 *
 * Author: gitlab.com/fejak
 */

#include <stdio.h>

int main ()
{
    char * s0  = "/*%c * Quine - a program that outputs its own source code.%c *%c * Author: ";
    char * s1  = "gitlab.com/fejak%c */%c%c#include <stdio.h>%c%cint main ()%c{%c    char * s0  ";
    char * s2  = "= %c%s%c;%c    char * s1  = %c%s%c;%c    char * s2  = %c%s%c;%c    char * s3  ";
    char * s3  = "= %c%s%c;%c    char * s4  = %c%s%c;%c    char * s5  = %c%s%c;%c    char * s6  ";
    char * s4  = "= %c%s%c;%c    char * s7  = %c%s%c;%c    char * s8  = %c%s%c;%c    char * s9  ";
    char * s5  = "= %c%s%c;%c    char * s10 = %c%s%c;%c    char * s11 = %c%s%c;%c    char * s12 ";
    char * s6  = "= %c%s%c;%c    char * s13 = %c%s%c;%c    char * s14 = %c%s%c;%c    char * s15 ";
    char * s7  = "= %c%s%c;%c    char * s16 = %c%s%c;%c    char * s17 = %c%s%c;%c    char * s18 ";
    char * s8  = "= %c%s%c;%c    char * s19 = %c%s%c;%c    char * s20 = %c%s%c;%c    printf ( s0";
    char * s9  = ", 10, 10, 10 );%c    printf ( s1, 10, 10, 10, 10, 10, 10, 10 );%c    printf ( ";
    char * s10 = "s2, 34, s0, 34, 10, 34, s1, 34, 10, 34, s2, 34, 10 );%c    printf ( s3, 34, s3";
    char * s11 = ", 34, 10, 34, s4, 34, 10, 34, s5, 34, 10 );%c    printf ( s4, 34, s6, 34, 10, ";
    char * s12 = "34, s7, 34, 10, 34, s8, 34, 10 );%c    printf ( s5, 34, s9, 34, 10, 34, s10, ";
    char * s13 = "34, 10, 34, s11, 34, 10 );%c    printf ( s6, 34, s12, 34, 10, 34, s13, 34, 10,";
    char * s14 = " 34, s14, 34, 10 );%c    printf ( s7, 34, s15, 34, 10, 34, s16, 34, 10, 34, ";
    char * s15 = "s17, 34, 10 );%c    printf ( s8, 34, s18, 34, 10, 34, s19, 34, 10, 34, s20, 34";
    char * s16 = ", 10 );%c    printf ( s9, 10, 10 );%c    printf ( s10, 10 );%c    printf ( s11,";
    char * s17 = " 10 );%c    printf ( s12, 10 );%c    printf ( s13, 10 );%c    printf ( s14, 10 ";
    char * s18 = ");%c    printf ( s15, 10 );%c    printf ( s16, 10, 10, 10 );%c    printf ( s17,";
    char * s19 = " 10, 10, 10 );%c    printf ( s18, 10, 10, 10 );%c    printf ( s19, 10, 10 );";
    char * s20 = "%c    printf ( s20, 10, 10, 10, 10 );%c    return 0;%c}%c";
    printf ( s0, 10, 10, 10 );
    printf ( s1, 10, 10, 10, 10, 10, 10, 10 );
    printf ( s2, 34, s0, 34, 10, 34, s1, 34, 10, 34, s2, 34, 10 );
    printf ( s3, 34, s3, 34, 10, 34, s4, 34, 10, 34, s5, 34, 10 );
    printf ( s4, 34, s6, 34, 10, 34, s7, 34, 10, 34, s8, 34, 10 );
    printf ( s5, 34, s9, 34, 10, 34, s10, 34, 10, 34, s11, 34, 10 );
    printf ( s6, 34, s12, 34, 10, 34, s13, 34, 10, 34, s14, 34, 10 );
    printf ( s7, 34, s15, 34, 10, 34, s16, 34, 10, 34, s17, 34, 10 );
    printf ( s8, 34, s18, 34, 10, 34, s19, 34, 10, 34, s20, 34, 10 );
    printf ( s9, 10, 10 );
    printf ( s10, 10 );
    printf ( s11, 10 );
    printf ( s12, 10 );
    printf ( s13, 10 );
    printf ( s14, 10 );
    printf ( s15, 10 );
    printf ( s16, 10, 10, 10 );
    printf ( s17, 10, 10, 10 );
    printf ( s18, 10, 10, 10 );
    printf ( s19, 10, 10 );
    printf ( s20, 10, 10, 10, 10 );
    return 0;
}
