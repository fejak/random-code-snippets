/*----------------------------------------------------------------------------*/
/*
 * A linked list of unique integer numbers with counting.
 *
 * Author: gitlab.com/fejak
 */
/*----------------------------------------------------------------------------*/
#include <stdio.h>      /* printf */
#include <stdlib.h>     /* malloc, EXIT_SUCCESS */
/*----------------------------------------------------------------------------*/
typedef struct TNode
{
    int     val_;
    int     cnt_;
    struct TNode * next_;
} TNode;
/*----------------------------------------------------------------------------*/
/*
    Malloc a new node, intialize its values and return the pointer.
 */
TNode * newNode ( int value )
{
    TNode * tmp = malloc ( sizeof ( TNode ) );
    tmp -> val_ = value;
    tmp -> cnt_ = 1;
    tmp -> next_ = NULL;
    return tmp;
}
/*----------------------------------------------------------------------------*/
/*
    Return value:
        -1 ...  The list was initialized - no node was present before.
         0 ...  A node of given value was not present before - was malloc'd.
         1 ...  A node of given value was already present - its counter was
                incremented.
*/
int insertValue ( TNode ** head, int value )
{
    TNode * ptr = NULL;

    /* head == NULL */
    if ( * head == NULL )
    {
        printf ( "Initializing the list... (%d)\n", value );
        * head = newNode ( value );
        return -1;
    }

    /* Check the header */
    if ( ( * head ) -> val_ == value )
    {
        printf ( "Incrementing the counter... (%d)\n", value );
        ++ ( ( * head ) -> cnt_ );
        return 1;
    }

    ptr = * head;

    /* iterate over the list until the end */
    while ( ptr -> next_ != NULL )
    {
        /* A node of given value is present - increment its counter */
        if ( ptr -> val_ == value )
        {
            printf ( "Incrementing the counter... (%d)\n", value );
            ++ ( ptr -> cnt_ );
            return 1;
        }
        ptr = ptr -> next_;
    }

    printf ( "Appending a new node... (%d)\n", value );
    ptr -> next_ = newNode ( value );
    
    return 0;
}
/*----------------------------------------------------------------------------*/
/*
    Print the values and their counts.
*/
void printList ( TNode * head )
{
    TNode * ptr = head;

    while ( ptr != NULL )
    {
        printf ( "%d - %dx\n", ptr -> val_, ptr -> cnt_ );
        ptr = ptr -> next_;
    }
}
/*----------------------------------------------------------------------------*/
int isEmpty ( TNode * head )
{
    return head == NULL;
}
/*----------------------------------------------------------------------------*/
/*
    Return value:
        -1 ...  Empty list.
         0 ...  The list is not sorted.
         1 ...  The list is sorted.
*/
int isSorted ( TNode * head )
{
    int prev;

    if ( head != NULL )
    {
        prev = head -> val_;
        head = head -> next_;
    }
    else
        return -1;

    while ( head != NULL )
    {
        if ( prev > head -> val_ )
            return 0;
        head = head -> next_;
    }

    return 1;
}
/*----------------------------------------------------------------------------*/
void swapHead ( TNode ** ptr )
{
    TNode * tmp = * ptr;
    * ptr = ( * ptr ) -> next_;
    tmp -> next_ = tmp -> next_ -> next_;
    ( * ptr ) -> next_ = tmp;
    tmp = NULL;
}
/*----------------------------------------------------------------------------*/
void swapNonHead ( TNode * ptr )
{
    TNode * tmp = ptr -> next_;
    ptr -> next_ = ptr -> next_ -> next_;
    tmp -> next_ = ptr -> next_ -> next_;
    ptr -> next_ -> next_ = tmp;
    tmp = NULL;
}
/*----------------------------------------------------------------------------*/
/*
    Sort the list using the naive bubble sort algorithm.

    Return value:
        -1 ...  The list is not initialized ( head == NULL ).
         0 ...  The list was already sorted.
         1 ...  The list was sorted using this algorithm.
 */
int sortList ( TNode ** head )
{
    int retVal = 0;
    int swapped;

    /* No element present - the list is not initialized */
    if ( * head == NULL )
        return -1;

    /* Only one element present => sorted */
    if ( ( * head ) -> next_ == NULL )
        return 0;

    /* Check the head and the next element */
    if ( ( * head ) -> val_ > ( * head ) -> next_ -> val_ )
    {
        swapHead ( head );
        retVal = 1;
    }

    do
    {
        TNode * ptr = * head;
        swapped = 0;

        /* Iterate over the linked list */
        while ( ptr -> next_ -> next_ != NULL )
        {
            if ( ptr -> next_ -> val_ > ptr -> next_ -> next_ -> val_ )
            {
                swapNonHead ( ptr );
                swapped = 1;
                retVal = 1;
            }
            ptr = ptr -> next_;
        }
    } while ( swapped );

    return retVal;
}
/*----------------------------------------------------------------------------*/
/*
    Insert into a sorted list.
    Prerequiste: An empty or a sorted list.
    Call sortList() before.
    
    Return value:
        -1 ...  The list was initialized - no node was present before.
         0 ...  A node of given value was not present before - was malloc'd.
         1 ...  A node of given value was already present - its counter was
                incremented.
 */
/*int insertValueSorted ( TNode ** head, int value )
{

}*/
/*----------------------------------------------------------------------------*/
/*
    Delete a node or decrease its counter.

    Return value:
        -2 ...  The list is not initialized ( head == NULL ).
        -1 ...  No node of given value was found.
         0 ...  A node of given value was found, its counter was greater than 1
                and therefore the counter was just decreased.
         1 ...  A node of given value was found, its counter was 1 and therefore
                the node was free'd.
         2 ...  Only the header node with the counter value of 1 was found =>
                the list was completely deleted.
*/
int deleteNode ( TNode ** head, int value )
{
    TNode * ptr = NULL;
  
    if ( * head == NULL )
        return -2;

    /* Check head */
    if ( ( * head ) -> val_ == value )
    {
        if ( ( * head ) -> cnt_ > 1 )
        {
            -- ( ( * head ) -> cnt_ );
            return 0;
        }
        else
        {
            TNode * tmp = ( * head ) -> next_;
            free ( * head );
            * head = tmp;
            if ( * head == NULL )
                return 2;
            else
                return 1;
        }
    }

    ptr = * head;

    /* Iterate over the list until the end */
    while ( ptr -> next_ != NULL )
    {
        if ( ptr -> next_ -> val_ == value )
        {
            if ( ptr -> next_ -> cnt_ > 1 )
            {
                -- ( ptr -> next_ -> cnt_ );
                return 0;
            }
            else /* ptr -> next_ -> cnt_ == 1 */
            {
                TNode * tmp = ptr -> next_ -> next_;
                free ( ptr -> next_ );
                ptr -> next_ = tmp;
                return 1;
            }
        }

        ptr = ptr -> next_;
    }

    return -1;
}
/*----------------------------------------------------------------------------*/
void deleteList ( TNode ** head )
{
    TNode * ptr = * head;

    while ( ptr != NULL )
    {
        ptr = ptr -> next_;
        free ( * head );
        * head = ptr;
    }

    * head = NULL;
}
/*----------------------------------------------------------------------------*/
void test1 ( void )
{
    TNode * head = NULL;
    int i, j;

    insertValue ( & head, 10 );
    insertValue ( & head, 10 );
    insertValue ( & head, 20 );
    insertValue ( & head, 20 );
    insertValue ( & head, 20 );
    insertValue ( & head, 30 );
    insertValue ( & head, 30 );
    
    insertValue ( & head, 15 );
    insertValue ( & head, 15 );
    insertValue ( & head, 10 );
    insertValue ( & head, 10 );
    insertValue ( & head, 5 );
    insertValue ( & head, 5 );

    printList ( head );

    deleteNode ( & head, 5 );
    deleteNode ( & head, 30 );
    deleteNode ( & head, 10 );
    deleteNode ( & head, 10 );
    deleteNode ( & head, 10 );
    deleteNode ( & head, 10 );
    
    printList ( head );


    for ( i = 0; i < 100; ++i )
        for ( j = 0; j < 10; ++j )
            deleteNode ( & head, i );

    printf ( "%d\n", isEmpty ( head ) );

    /*deleteList ( & head );*/

    printf ( "%d\n", isSorted ( head ) );
    
    insertValue ( & head, 15 );
    insertValue ( & head, 15 );
    insertValue ( & head, 16 );
    insertValue ( & head, 17 );
    insertValue ( & head, 15 );
    insertValue ( & head, 16 );
    insertValue ( & head, 5 );
    printf ( "%d\n", isSorted ( head ) );

    printList ( head );
    
    sortList ( & head );
    
    printList ( head );

    deleteList ( & head );
}
/*----------------------------------------------------------------------------*/
void test2 ( void )
{
    TNode * head = NULL;

    sortList ( & head );

    insertValue ( & head, 10 );
    
    sortList ( & head );
    
    insertValue ( & head, 2 );
    insertValue ( & head, 7 );
    insertValue ( & head, 5 );
    insertValue ( & head, 3 );
    insertValue ( & head, 7 );
   
    printList ( head );

    sortList ( & head );
    
    printf ( "\n" );

    printList ( head );
    
    printf ( "\n" );
    
    deleteNode ( & head, 10 );
    deleteNode ( & head, 3 );
    deleteNode ( & head, 2 );
    
    printList ( head );

    deleteList ( & head );
}
/*----------------------------------------------------------------------------*/
int main ( void )
{
    /*test1 ();*/

    test2 ();

    return EXIT_SUCCESS;
}
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
