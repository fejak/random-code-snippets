//-----------------------------------------------------------------------------
/*
 * Read two time entries in correct format and print their difference.
 * Input: two time entires in the format hh:mm:ss.
 * Compiled with 'gcc -Wall -pedantic -Wextra -std=c99 -O0'
 *
 * Author: gitlab.com/fejak
 *
 * TODO: fix input ???
 */
//-----------------------------------------------------------------------------
#include <stdio.h>      // printf, scanf
#include <stdlib.h>     // EXIT_SUCCESS, EXIT_FAILURE
#include <stdbool.h>    // bool, true, false
//-----------------------------------------------------------------------------
bool isDigit ( char c )
{
  return c > 47 && c < 58;
}
//-----------------------------------------------------------------------------
/**
 * Convert the two chars into one integer time element, assign this time
 * element to the output parameter and assert it's lesser than the limit.
 */
bool parseTimeElement ( char c1, char c2, int * elem, int limit )
{
  * elem = ( c1 - 48 ) * 10 + ( c2 - 48 );

  return isDigit ( c1 )
      && isDigit ( c2 )
      && * elem < limit;
}
//-----------------------------------------------------------------------------
bool parseTimeEntry ( char c [], int input [] )
{
  return parseTimeElement ( c[0], c[1], &input [0], 24 )    // Hours
      && c[2] == ':'
      && parseTimeElement ( c[3], c[4], &input [1], 60 )    // Minutes
      && c[5] == ':'
      && parseTimeElement ( c[6], c[7], &input [2], 60 );   // Seconds
}
//-----------------------------------------------------------------------------
/**
 * Read one time entry, with a prompt.
 */
bool readInput ( int input [], int i )
{
  char c [8];

  printf ( "Enter time %d: (hh:mm:ss)\n", i );

  return scanf ( " %c%c%c%c%c%c%c%c", &c[0], &c[1], &c[2], &c[3],
                                      &c[4], &c[5], &c[6], &c[7] ) == 8
      && parseTimeEntry ( c, input );
}
//-----------------------------------------------------------------------------
/**
 * Read the two time entries separated by whitespace(s).
 */
bool getInput ( int input [][3] )
{
  return readInput ( input [0], 1 )
      && readInput ( input [1], 2 );
}
//-----------------------------------------------------------------------------
/**
 * Convert the time in the format of hours/minutes/seconds into seconds only.
 */
int toSecs ( int time [] )
{
  return time [0] * 3600 + time [1] * 60 + time [2];
}
//-----------------------------------------------------------------------------
void secsToHMS ( int secs, int * h, int * m, int * s )
{
  * h = secs / 3600;
  secs %= 3600;
  * m = secs / 60;
  secs %= 60;
  * s = secs;
}
//-----------------------------------------------------------------------------
/**
 * Print the difference between the two time entries.
 * If time2 < time1, then time2 is considered "tomorrow".
 */
void printDifference ( int time1 [], int time2 [] )
{
  int h, m, s;
  int secs1 = toSecs ( time1 );
  int secs2 = toSecs ( time2 );
  int diff  = secs2 >= secs1 ? secs2 - secs1 : 24 * 3600 - secs1 + secs2;

  secsToHMS ( diff, &h, &m, &s );

  printf ( "The difference is:\n%02d:%02d:%02d\n", h, m, s );
}
//-----------------------------------------------------------------------------
int main ( void )
{
  int input [2][3];

  if ( ! getInput ( input ) )
  {
    printf ( "Input error!\n" );
    return EXIT_FAILURE;
  }

  printDifference ( input [0], input [1] );

  return EXIT_SUCCESS;
}
//-----------------------------------------------------------------------------
