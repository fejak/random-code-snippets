/*---------------------------------------------------------------------------*/
/**
 * Trim trailing whitespaces (spaces / tabs) in a text file.
 * This version reads from standard input (for files it's possible to redirect
 * input / output) or uses the command line parameter as an input file name.
 * This version uses a resizable "vector" implementation - probably an overkill
 * but this is just for fun / demonstration.
 *
 * Compiled with 'gcc -Wall -pedantic -Wextra -std=c89 -O0 trimTrailing.c'
 * Author: gitlab.com/fejak
 *
 * WARNING - DO NOT USE THIS PROGRAM ON BINARY FILES - POTENTIALLY DESTRUCTIBLE
 */
/*---------------------------------------------------------------------------*/
#include <stdio.h>      /*  printf, fprintf, fgetc, fopen, fclose */
#include <stdlib.h>     /*  EXIT_FAILURE, EXIT_SUCCESS, malloc, realloc */
#include <string.h>     /*  strerror */
#include <time.h>       /*  time, localtime, strftime */
#include <errno.h>      /*  errno */
/*---------------------------------------------------------------------------*/
#define INIT_SIZE         1     /* Initial size of the vector */
#define RESIZE_FACTOR     2     /* Resize factor of every reallocation */
/*---------------------------------------------------------------------------*/
#define BOOL              int
#define TRUE              1
#define FALSE             0
/*---------------------------------------------------------------------------*/
#define TIME_STR_LEN      64    /* Length of the time string array */
#define FILE_SUFFIX       ".old_"
#define TIME_FORMAT       "%y%m%d%H%M%S"
/*---------------------------------------------------------------------------*/
/**
 * "Type vector-char"
 */
typedef struct TVecChar
{
  char * m_Ptr;     /* Pointer to the char array */
  int m_Alloc;      /* Amount of allocated char array */
  int m_Len;        /* Actual occupied space of the char array */
} TVecChar;
/*---------------------------------------------------------------------------*/
BOOL              isSpaceTab              ( char                c )
{
  return c == ' ' || c == '\t';
}
/*---------------------------------------------------------------------------*/
/**
 * Initialize the vector.
 */
void              vectorInit              ( TVecChar          * arr )
{
  arr -> m_Ptr = NULL;
  arr -> m_Alloc = 0;
  arr -> m_Len = 0;
}
/*---------------------------------------------------------------------------*/
/**
 * This function handles reallocation of the vector.
 */
void              vectorAlloc             ( TVecChar          * arr )
{
  if ( arr -> m_Alloc == 0 )
  {
    arr -> m_Alloc = INIT_SIZE;
    arr -> m_Ptr = malloc ( sizeof ( * ( arr -> m_Ptr ) ) * arr -> m_Alloc );
  }
  else
  if ( arr -> m_Alloc == arr -> m_Len )
  {
    arr -> m_Alloc *= RESIZE_FACTOR;
    arr -> m_Ptr = realloc ( arr -> m_Ptr,
                              sizeof ( * ( arr -> m_Ptr ) ) * arr -> m_Alloc );
  }
}
/*---------------------------------------------------------------------------*/
/**
 * Store a character into the vector.
 * Always check whether reallocation is needed (handled in vectorAlloc).
 */
void              vectorStore             ( TVecChar          * arr,
                                            char                c )
{
  vectorAlloc ( arr );
  arr -> m_Ptr [ arr -> m_Len ] = c;
  ++ ( arr -> m_Len );
}
/*---------------------------------------------------------------------------*/
/**
 * Print the content of the vector.
 */
void              vectorPrint             ( TVecChar  const   * arr,
                                            FILE              * outFile )
{
  int i;

  for ( i = 0; i < arr -> m_Len; ++i )
  {
    fprintf ( outFile, "%c", arr -> m_Ptr [i] );
  }
}
/*---------------------------------------------------------------------------*/
/**
 * Reset the vector to its initial state.
 */
void              vectorReset             ( TVecChar          * arr )
{
  free ( arr -> m_Ptr );
  vectorInit ( arr );
}
/*---------------------------------------------------------------------------*/
/**
 * Read from stdin until EOF.
 * If encounter a space/tab, store it into the vector.
 * If there's a non-whitespace character on the same line, print the content
 * of the vector before it.
 * If there's no non-whitespace character on the same line anymore (except
 * newline), don't print anything (except the newline) and reset the vector.
 */
void              readInput               ( TVecChar          * arr,
                                            FILE              * inFile,
                                            FILE              * outFile )
{
  while ( TRUE )
  {
    int c = fgetc ( inFile );

    if ( c == EOF )
    {
      break;
    }
    else
    if ( isSpaceTab ( c ) )
    {
      vectorStore ( arr, c );
    }
    else
    {
      if ( c != '\n' )
      {
        vectorPrint ( arr, outFile );
      }
      fprintf ( outFile, "%c", c );

      vectorReset ( arr );
    }
  }
}
/*---------------------------------------------------------------------------*/
/**
 * Get the current time stamp in a string format suitable for a file name.
 * Generates time stamp in the format:
 *   YearMonthDayHourMinuteSecond
 */
void              getTimeStr              ( char              * timeStr,
                                            int                 length )
{
  /* Get the current time */
  time_t t = time ( NULL );
  /* Format the time into a string according to the TIME_FORMAT formatter */
  strftime ( timeStr, length, TIME_FORMAT, localtime ( &t ) );
}
/*---------------------------------------------------------------------------*/
void              vectorAppend            ( TVecChar          * arr,
                                            char      const   * str )
{
  int i = 0;

  while ( str [i] != '\0' )
  {
    vectorStore ( arr, str [i] );
    ++i;
  }
}
/*---------------------------------------------------------------------------*/
/**
 * Derive the backup file name from the original file name.
 */
void              getBackupFileName       ( char      const   * origFileName,
                                            TVecChar          * bckpFileName )
{
  char timeStr [TIME_STR_LEN];

  getTimeStr ( timeStr, TIME_STR_LEN );

  vectorAppend ( bckpFileName, origFileName );
  vectorAppend ( bckpFileName, FILE_SUFFIX );
  vectorAppend ( bckpFileName, timeStr );
  vectorStore  ( bckpFileName, '\0' );
}
/*---------------------------------------------------------------------------*/
void              copyFile                ( FILE              * inFile,
                                            FILE              * outFile )
{
  while ( TRUE )
  {
    int c = fgetc ( inFile );

    if ( c == EOF )
    {
      break;
    }

    fputc ( c, outFile );
  }
}
/*---------------------------------------------------------------------------*/
/**
 * 1) Make a backup of the original input file.
 * 2) Use this backup as an input for creation of the new, trimmed file.
 */
BOOL              handleFiles             ( FILE             ** inFile,
                                            FILE             ** outFile,
                                            char      const   * origFileName )
{
  TVecChar bckpFileName;
  vectorInit ( & bckpFileName );

  /* Open the input file for reading */
  if ( ( * inFile = fopen ( origFileName, "r" ) ) == NULL )
  {
    vectorReset ( & bckpFileName );
    return FALSE;
  }

  getBackupFileName ( origFileName, & bckpFileName );

  /* Open the backup file for writing */
  if ( ( * outFile = fopen ( bckpFileName . m_Ptr, "w" ) ) == NULL )
  {
    fclose ( * inFile );
    vectorReset ( & bckpFileName );
    return FALSE;
  }

  copyFile ( * inFile, * outFile );

  fclose ( * inFile );
  fclose ( * outFile );

  /* Open the backup file for reading */
  if ( ( * inFile = fopen ( bckpFileName . m_Ptr, "r" ) ) == NULL )
  {
    vectorReset ( & bckpFileName );
    return FALSE;
  }

  /* Open the original input file for writing */
  if ( ( * outFile = fopen ( origFileName, "w" ) ) == NULL )
  {
    fclose ( * inFile );
    vectorReset ( & bckpFileName );
    return FALSE;
  }

  vectorReset ( & bckpFileName );
  return TRUE;
}
/*---------------------------------------------------------------------------*/
BOOL              handleOpen              ( int                 argc,
                                            char              * argv [],
                                            FILE             ** inFile,
                                            FILE             ** outFile )
{
  if ( argc < 2 )
  {
    * inFile  = stdin;
    * outFile = stdout;
    return TRUE;
  }

  return handleFiles ( inFile, outFile, argv [1] );
}
/*---------------------------------------------------------------------------*/
void              handleClose             ( FILE             ** inFile,
                                            FILE             ** outFile )
{
  if ( * inFile != stdin )
  {
    fclose ( * inFile );
    fclose ( * outFile );
    * inFile  = NULL;
    * outFile = NULL;
  }
}
/*---------------------------------------------------------------------------*/
int               main                    ( int                 argc,
                                            char              * argv [] )
{
  TVecChar arr;

  FILE * inFile  = NULL;
  FILE * outFile = NULL;

  if ( ! handleOpen ( argc, argv, & inFile, & outFile ) )
  {
    printf ( "Error: %s\n", strerror ( errno ) );
    return EXIT_FAILURE;
  }

  vectorInit ( & arr );

  readInput ( & arr, inFile, outFile );

  vectorReset ( & arr );

  handleClose ( & inFile, & outFile );

  return EXIT_SUCCESS;
}
/*---------------------------------------------------------------------------*/
