/**
 * The Sieve of Eratosthenes using bits / bit masks.
 *
 * Author: gitlab.com/fejak
 */
/*----------------------------------------------------------------------------*/
#include <stdio.h>

#define LIMIT   10000 /* 10 kB => 80.000 numbers */
/*----------------------------------------------------------------------------*/
int power ( int base, int exp )
{
    int i;
    int res = 1;

    for ( i = 0; i < exp; ++i )
    {
        res *= base;
    }

    return res;
}
/*----------------------------------------------------------------------------*/
void markNonPrime ( unsigned char * arr, int i )
{
    int idx = i / 8;
    int ofs = i % 8;

    unsigned char mask = power ( 2, ofs );

    arr [idx] |= mask;
}
/*----------------------------------------------------------------------------*/
/* Mark the base number as prime and all its multiples as non-primes. */
void markPrime ( unsigned char * arr, int const base )
{
    int mult = base * 2;

    if ( mult >= LIMIT * 8 )
        return;

    while ( 1 )
    {
        markNonPrime ( arr, mult );

        mult += base;

        if ( mult >= LIMIT * 8 )
            break;
    }
}
/*----------------------------------------------------------------------------*/
void printPrimes ( unsigned char * arr )
{
    int idx;

    for ( idx = 0; idx < LIMIT; ++ idx )
    {
        int ofs;

        for ( ofs = 0; ofs < 8; ++ ofs )
        {
            unsigned char mask = power ( 2, ofs );

            if ( ( arr [idx] & mask ) == 0 )
            {
                printf ( "%d\n", idx * 8 + ofs );
            }
        }
    }
}
/*----------------------------------------------------------------------------*/
void findNextUnmarked ( unsigned char * arr, int * base )
{
    int idxInit = ( * base ) / 8;
    int ofsInit = ( * base ) % 8;

    int idx, ofs;

    for ( ofs = ofsInit; ofs < 8; ++ ofs )
    {
        unsigned char mask = power ( 2, ofs );

        if ( ( arr [idxInit] & mask ) == 0 )
        {
            * base = idxInit * 8 + ofs;
            return;
        }
    }

    for ( idx = idxInit + 1; idx < LIMIT; ++ idx )
    {
        for ( ofs = 0; ofs < 8; ++ ofs )
        {
            unsigned char mask = power ( 2, ofs );

            if ( ( arr [idx] & mask ) == 0 )
            {
                * base = idx * 8 + ofs;
                return;
            }
        }
    }
}
/*----------------------------------------------------------------------------*/
int main ( void )
{
    unsigned char arr [LIMIT] = {0};
    int base = 2;

    markNonPrime ( arr, 0 );
    markNonPrime ( arr, 1 );

    while ( 1 )
    {
        markPrime ( arr, base );

        base += 1;

        if ( base >= LIMIT * 8 )
            break;

        findNextUnmarked ( arr, & base );

        if ( base >= LIMIT * 8 )
            break;
    }

    printPrimes ( arr );

    return 0;
}
