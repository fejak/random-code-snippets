/*----------------------------------------------------------------------------*/
/*
 * A primitive approximation of square root calculation using iteration.
 * Compilable with 'gcc -Wall -pedantic -Wextra -std=c89 -O0'
 * Author: gitlab.com/fejak
 */
/*----------------------------------------------------------------------------*/
#include <stdio.h>
/*----------------------------------------------------------------------------*/
#define LIMIT         1000000
#define EPSILON       .000000001    /* Tolerance */
#define FACTOR        2
/*----------------------------------------------------------------------------*/
double squareRoot ( int const x )
{
  double a = 1;     /* Base square root value */
  double inc = 1;   /* Base increment value */

  while ( 1 )
  {
    /* "Overflow" => step back + decrease increment */
    if ( a * a - x > EPSILON )
    {
      a -= inc;
      inc /= FACTOR;
    }
    else
    /* "Underflow" => add current increment to the square root value */
    if ( x - a * a > EPSILON )
    {
      a += inc;
    }
    else
    /* Square root is in the EPSILON tolerance */
    {
      return a;
    }
  }
}
/*----------------------------------------------------------------------------*/
int main ( void )
{
  int i;

  for ( i = 0; i <= LIMIT; ++i )
  {
    printf ( "sqrt ( %d ) = %.15f\n", i, squareRoot ( i ) );
  }

  return 0;
}
/*----------------------------------------------------------------------------*/
