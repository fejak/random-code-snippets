/*---------------------------------------------------------------------------*/
/**
 * Allocate an array, fill it with random values and print the array
 * recursively.
 * Compiled with 'gcc -Wall -pedantic -Wextra -std=c89 -O0'
 *
 * Author: gitlab.com/fejak
 */
/*---------------------------------------------------------------------------*/
#include <stdio.h>    /* printf */
#include <stdlib.h>   /* srand, malloc, rand, free, EXIT_SUCCESS */
#include <time.h>     /* time */
/*---------------------------------------------------------------------------*/
#define ARR_SIZE      25    /* Size of the array */
#define EL_RANGE      100   /* Range of random-generated elements */
/*---------------------------------------------------------------------------*/
void allocateArray ( int ** arr, int size )
{
  ( * arr ) = malloc ( sizeof ( ** arr ) * size );
}
/*---------------------------------------------------------------------------*/
void freeArray ( int ** arr )
{
  free ( * arr );
  * arr = NULL;
}
/*---------------------------------------------------------------------------*/
void fillArray ( int * arr, int size, int elRange )
{
  int i;

  for ( i = 0; i < size; ++i )
  {
    arr [i] = rand () % elRange;
  }
}
/*---------------------------------------------------------------------------*/
/**
 * Principle:
 *   In every recursive step print the leftmost element of the array.
 *   Then move the pointer to the right by one and decrease the length
 *   of the array by one.
 *   When the length of the remaining array equals zero, stop the recursion.
 */
void printArrayRec ( int const * arr, int size )
{
  if ( size == 0 )
  {
    printf ( "\n" );
    return;
  }

  printf ( "%d", * arr );

  if ( size > 1 )
    printf ( " " );

  printArrayRec ( arr + 1, size - 1 );
}
/*---------------------------------------------------------------------------*/
int main ( void )
{
  int * arr = NULL;
  int size = ARR_SIZE;
  int elRange = EL_RANGE;

  srand ( time ( NULL ) );

  allocateArray ( & arr, size );

  fillArray ( arr, size, elRange );

  printArrayRec ( arr, size );

  freeArray ( & arr );

  return EXIT_SUCCESS;
}
/*---------------------------------------------------------------------------*/
