//-----------------------------------------------------------------------------
/**
 * Convert non-negative decimal int representation into binary using recursion.
 * Converts all numbers from 0 to given argument.
 * Compiled with 'gcc -Wall -pedantic -Wextra -std=c99 -O0 decToBin.c -lm'
 * Author: gitlab.com/fejak
 */
//-----------------------------------------------------------------------------
#include <stdio.h>    // printf
#include <stdlib.h>   // EXIT_FAILURE, EXIT_SUCCESS
#include <math.h>     // log10
#include <stdbool.h>  // bool, true, false
//-----------------------------------------------------------------------------
/**
 * Power function for positive integers.
 */
int power ( int base, int exp )
{
  if ( exp == 0 )
    return 1;

  int res = base;

  for ( int i = 1; i < exp; ++i )
  {
    res *= base;
  }

  return res;
}
//-----------------------------------------------------------------------------
/**
 * Print the binary representation of the number one binary digit at a time.
 */
void decToBinRec ( int number, int order )
{
  // Base case - no more order to print => return
  if ( order == -1 )
  {
    return;
  }

  // Number to subtract
  int sub = power ( 2, order );

  if ( number >= sub )
  {
    printf ( "1" );
    number -= sub;
  }
  else
  {
    printf ( "0" );
  }

  decToBinRec ( number, order - 1 );
}
//-----------------------------------------------------------------------------
/**
 * Proxy / initialization function for the recursion.
 */
void decToBin ( int number )
{
  // The highest order of the binary form of the number
  int hiOrder;

  if ( number == 0 )
  {
    hiOrder = 0;
  }
  else
  {
    // Emulation of the logarithm base 2
    hiOrder = log10 ( number ) / log10 ( 2 );
  }

  decToBinRec ( number, hiOrder );
}
//-----------------------------------------------------------------------------
/**
 * "Pretty print" function.
 */
void printDecToBin ( int number )
{
  printf ( "%d (dec) = ", number );
  decToBin ( number );
  printf ( " (bin)\n" );
}
//-----------------------------------------------------------------------------
int main ( int argc, char * argv [] )
{
  int limit;

  if ( argc < 2
      || sscanf ( argv[1], "%d", &limit ) != 1
      || limit < 0 )
  {
    printf ( "Usage: program [non-negative_integer]\n" );
    return EXIT_FAILURE;
  }

  for ( int i = 0; i <= limit; ++i )
  {
    printDecToBin ( i );
  }

  return EXIT_SUCCESS;
}
//-----------------------------------------------------------------------------
