/*---------------------------------------------------------------------------*/
/*
 * Count the decimal digits of a given integer number using recursion.
 * Compiled with 'gcc -Wall -pedantic -Wextra -std=c89 -O0 countDigits.c'
 * Author: gitlab.com/fejak
 */
/*---------------------------------------------------------------------------*/
#include <stdio.h>    /* printf, scanf */
#include <stdlib.h>   /* EXIT_FAILURE, EXIT_SUCCESS */
/*---------------------------------------------------------------------------*/
/*
 * Count the digits recursively
 *
 * Principle:
 *   By dividing the number by 10 in every recursive step, we are effectively
 *   "slashing" the number from the right and thus counting the digits from
 *   right to left.
 */
long int countDigitsRec ( long int input )
{
  /* Base case - no decimal order remains */
  if ( input == 0 )
    return 0;

  /* Recursive step - a decimal order remains - add 1 to the sum
     and slash the number by one digit, effectively moving
     by one decimal order to the left */
  return 1 + countDigitsRec ( input / 10 );
}
/*---------------------------------------------------------------------------*/
/* Proxy function for the recursion */
long int countDigits ( long int input )
{
  if ( input == 0 )
    return 1;

  return countDigitsRec ( input );
}
/*---------------------------------------------------------------------------*/
int main ( void )
{
  long int input;

  printf ( "Enter an integer: " );

  if ( scanf ( " %ld", &input ) != 1 )
  {
    printf ( "Input error.\n" );
    return EXIT_FAILURE;
  }

  printf ( "Number of digits is: %ld\n", countDigits ( input ) );

  return EXIT_SUCCESS;
}
/*---------------------------------------------------------------------------*/
