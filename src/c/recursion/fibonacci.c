//-----------------------------------------------------------------------------
/**
 * Print the first n elements of the Fibonacci sequence.
 * Efficient version - linear / tail recursion.
 * Compiled with 'gcc -Wall -pedantic -Wextra -std=c99 -O0'
 * Internal behavior of the recursion depends on the optimization flag value.
 *
 * Author: gitlab.com/fejak
 */
//-----------------------------------------------------------------------------
#include <stdio.h>    // printf
#include <stdlib.h>   // EXIT_FAILURE, EXIT_SUCCESS
#include <stdbool.h>  // bool, false, true
//-----------------------------------------------------------------------------
#define LIMIT     50
//-----------------------------------------------------------------------------
/**
 * When using the gcc optimization level >0, the tail call will be converted
 * ("optimized") to iteration => no stack overflow will occur even when using
 * large values of LIMIT.
*/
void printFibonacciRec ( long long prevPrev, long long prev, int n, int idx )
{
  if ( n <= 1 )
    return;

  long long curr = prev + prevPrev;

  printf ( "%d: %lld\n", idx, curr );

  printFibonacciRec ( prev, curr, n - 1, idx + 1 );
}
//-----------------------------------------------------------------------------
/**
 * Proxy / init function for the recursion.
 */
bool printFibonacci ( int n )
{
  if ( n < 0 )
    return false;

  printf ( "0: 0\n" );

  if ( n > 0 )
    printf ( "1: 1\n" );

  printFibonacciRec ( 0, 1, n, 2 );

  return true;
}
//-----------------------------------------------------------------------------
int main ( void )
{
  if ( ! printFibonacci ( LIMIT ) )
  {
    printf ( "Limit error.\n" );
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
//-----------------------------------------------------------------------------
