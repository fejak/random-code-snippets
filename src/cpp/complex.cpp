//------------------------------------------------------------------------------
/**
 * Write a class implementing complex numbers and their basic operations.
 * This is one of the "canonical" C++ exercises.
 * Compilable with: 'g++ -Wall -Wextra -pedantic -std=c++03 -O0 complex.cpp'
 *
 * Author: gitlab.com/fejak
 * TODO: Add more tests.
 */
//------------------------------------------------------------------------------
#include <iostream>
#include <cmath>
#include <sstream>
#include <cassert>
//==============================================================================
class CComplex
{
  public:
    // Parametrized + default constructor
    CComplex ( double re = 0, double im = 0 );
    // Absolute value of the complex number
    double Abs () const;
    // Arithmetic operators
    CComplex operator + ( CComplex const & obj ) const;
    CComplex operator - ( CComplex const & obj ) const;
    CComplex operator * ( CComplex const & obj ) const;
    // Relational operators
    bool operator == ( CComplex const & obj ) const;
    bool operator != ( CComplex const & obj ) const;
    bool operator <  ( CComplex const & obj ) const;
    bool operator >  ( CComplex const & obj ) const;
    bool operator <= ( CComplex const & obj ) const;
    bool operator >= ( CComplex const & obj ) const;
  private:
    double m_Re;  // Real part of the number
    double m_Im;  // Imaginary part of the number
    friend CComplex operator * ( double a, const CComplex & obj );
    friend std::ostream & operator << ( std::ostream & os, const CComplex & obj );
};
//------------------------------------------------------------------------------
CComplex::CComplex ( double re, double im )
  : m_Re ( re ), m_Im ( im )
{}
//------------------------------------------------------------------------------
double CComplex::Abs () const
{
  return std::sqrt ( std::pow ( m_Re, 2 ) + std::pow ( m_Im, 2 ) );
}
//------------------------------------------------------------------------------
CComplex CComplex::operator + ( CComplex const & obj ) const
{
  return CComplex ( m_Re + obj . m_Re, m_Im + obj . m_Im );
}
//------------------------------------------------------------------------------
CComplex CComplex::operator - ( CComplex const & obj ) const
{
  return CComplex ( m_Re - obj . m_Re, m_Im - obj . m_Im );
}
//------------------------------------------------------------------------------
CComplex CComplex::operator * ( CComplex const & obj ) const
{
  return CComplex ( m_Re * obj . m_Re - m_Im * obj . m_Im,
                    m_Re * obj . m_Im + m_Im * obj . m_Re );
}
//------------------------------------------------------------------------------
bool CComplex::operator == ( CComplex const & obj ) const
{
  return m_Re == obj . m_Re
      && m_Im == obj . m_Im;
}
//------------------------------------------------------------------------------
bool CComplex::operator != ( CComplex const & obj ) const
{
  return ! ( * this == obj );
}
//------------------------------------------------------------------------------
bool CComplex::operator < ( CComplex const & obj ) const
{
  return Abs () < obj . Abs ();
}
//------------------------------------------------------------------------------
bool CComplex::operator > ( CComplex const & obj ) const
{
  return obj < * this;
}
//------------------------------------------------------------------------------
bool CComplex::operator <= ( CComplex const & obj ) const
{
  return ! ( * this > obj );
}
//------------------------------------------------------------------------------
bool CComplex::operator >= ( CComplex const & obj ) const
{
  return ! ( * this < obj );
}
//------------------------------------------------------------------------------
CComplex operator * ( double a, CComplex const & obj )
{
  return CComplex ( a * obj . m_Re, a * obj . m_Im );
}
//------------------------------------------------------------------------------
std::ostream & operator << ( std::ostream & os, CComplex const & obj )
{
  if ( obj . m_Re == 0 )
  {
    os  << obj . m_Im;
  }
  else
  {
    os  << obj . m_Re;
    if ( obj . m_Im != 0 )
    {
      os  << ( obj . m_Im < 0 ? " - " : " + " )
          << std::fabs ( obj . m_Im );
    }
  }

  if ( obj . m_Im != 0 )
    os << "i";
  
  return os;
}
//==============================================================================
void assertStr ( CComplex const & obj, std::string const & str )
{
  std::stringstream ss;

  ss << obj;

  assert ( ss . str () == str );
}
//------------------------------------------------------------------------------
void test1 ()
{
  CComplex n1 ( -6, 0 );
  CComplex n2 ( 10, 20 );
  CComplex n3 ( 0, -5 );
  CComplex n4;

  assertStr ( n1, "-6" );
  assertStr ( n2, "10 + 20i" );
  assertStr ( n3, "-5i" );
  assertStr ( n4, "0" );

  assert ( n1 . Abs () == 6 );

  // TODO: More tests
}
//------------------------------------------------------------------------------
int main ()
{
  test1 ();

  return 0;
}
//------------------------------------------------------------------------------
